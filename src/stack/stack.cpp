#include "../utils/GameUtils.h"
#include "../utils/GUtils.h"
#include "../food/food.h"
#include "stack.h"
#include <iostream>

Food Stack::foods[3];
Food Stack::deliveryFoods[6];
int Stack::top = -1;
int Stack::front = -1;
int Stack::stack[3];
bool Stack::isAnimatingPush = false;
bool Stack::isAnimatingPop = false;

void Stack::reset()
{
    // Pops all items in the stack 
    
    while (Stack::popStack(false));

    // Hides all items in the queue

    for(int i=0; i<6;i++){
        deliveryFoods[i].foodID=0;
    }
}

void Stack::pushToQueue(int i)
{
    front++;
    front = front % 6;
    deliveryFoods[front].x = foods[i].x;
    deliveryFoods[front].y = foods[i].y;
    deliveryFoods[front].foodID = foods[i].foodID;
    deliveryFoods[front].onConveyor = false;
    deliveryFoods[front].isDrawn = true;
    deliveryFoods[front].isQueueIntermediate = true;
    deliveryFoods[front].onQueue = false;
    isAnimatingPop = true;
}

bool Stack::popToQueue()
{
    if (top < 0)
    {
        std::cout << "Stack underflow" << std::endl;
        return false;
    }
    if (isAnimatingPush == false)
    {
        pushToQueue(top);
        foods[top].foodID = 0;
        foods[top].onConveyor = false;
        foods[top].isDrawn = false;
        foods[top].isIntermediate = false;
        foods[top].onStack = false;
        foods[top].stackAnimStep = false;
        isAnimatingPush = false;
        std::cout << stack[top] << " was popped" << std::endl;
        GameUtils::foods[stack[top]].reload();
        top--;
        return true;
    }
    return false;
}

bool Stack::pushStack(int i)
{
    if (top >= 2)
    {
        std::cout << "Stack overflow" << std::endl;
        return false;
    }

    GameUtils::foods[i].toggleConveyor();
    top++;
    stack[top] = i;
    foods[top].x = GameUtils::foods[i].x;
    foods[top].y = GameUtils::foods[i].y;
    foods[top].foodID = GameUtils::foods[i].getID();
    foods[top].onConveyor = false;
    foods[top].isDrawn = true;
    foods[top].isIntermediate = true;
    isAnimatingPush = true;
    return true;
}

bool Stack::popStack(bool q)
{
    if (top < 0)
    {
        std::cout << "Stack underflow" << std::endl;
        return false;
    }
    if (q == false)
    {

        foods[top].foodID = 0;
        foods[top].onConveyor = false;
        foods[top].isDrawn = false;
        foods[top].isIntermediate = false;
        foods[top].onStack = false;
        foods[top].stackAnimStep = false;
        isAnimatingPush = false;
        std::cout << stack[top] << " was popped" << std::endl;
        GameUtils::foods[stack[top]].foodID = GameUtils::foods[stack[top]].savedID;
        top--;
        return true;
    }
    else
    {
        return popToQueue();
    }
    return false;
}
GLfloat disp[6] = {
    GUtils::prepareConveyorScale(1.5),
    GUtils::prepareConveyorScale(1.0),
    GUtils::prepareConveyorScale(0.5),
    0, 0, 0};

void Stack::render()
{
    for (int i = 0; i < 6; i++)
    {
        if (deliveryFoods[i].isQueueIntermediate == true && deliveryFoods[i].onQueue == false)
        {
            deliveryFoods[i].isDrawn = true;
            if (deliveryFoods[i].moveTo(1.75 + disp[i % 3], 1.7) == true)
            {
                isAnimatingPop = false;
                deliveryFoods[i].isQueueIntermediate = false;
                deliveryFoods[i].onQueue = true;
            }
        }
        if (deliveryFoods[i].isQueueIntermediate == false && deliveryFoods[i].onQueue == true)
        {
            if (!(deliveryFoods[i].x > (GLfloat)GUtils::windowWidth / (GLfloat)2 + 0.2))
            {
                deliveryFoods[i].move();
                deliveryFoods[i].isQueueIntermediate = false;
                deliveryFoods[i].onQueue = true;
            }
            else
            {
                deliveryFoods[i].isIntermediate = false;
                deliveryFoods[i].onStack = false;
                deliveryFoods[i].onConveyor = false;
                deliveryFoods[i].isQueueIntermediate = false;
                deliveryFoods[i].foodID = 0;
                deliveryFoods[i].isDrawn = false;
                deliveryFoods[i].onQueue = false;
            }
        }
        deliveryFoods[i].render();
    }

    for (int i = 0; i < 3; i++)
    {

        if (foods[i].isIntermediate == true && foods[i].onStack == false && foods[i].stackAnimStep == false && foods[i].onConveyor == false)
        {
            if (foods[i].moveTo(2, 0.5 + 0.4 * (top)) == true)
            {
                foods[i].stackAnimStep = true;
            }
        }
        else if (foods[i].stackAnimStep == true)
        {
            if (i == 0)
            {
                if (foods[i].moveTo(2, -1.7) == true)
                {
                    foods[i].onStack = true;
                    foods[i].stackAnimStep = false;
                    isAnimatingPush = false;
                }
            }
            else if (foods[i].moveTo(2, 0.5 + foods[i - 1].y) == true)
            {
                foods[i].onStack = true;
                foods[i].stackAnimStep = false;
                isAnimatingPush = false;
            }
        }

        foods[i].render();
    }
}