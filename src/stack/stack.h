#ifndef STACK
#define STACK
#include "GL/gl.h"
#include "../utils/GUtils.h"
#include "../food/food.h"

class Stack
{
public:
  static Food foods[3];
  static Food deliveryFoods[6];
  static bool pushStack(int i);
  static bool popStack(bool q);
  static void pushToQueue(int i);
  static bool popToQueue();
  static void reset();
  static int stack[3];
  static int top;
  static int front;
  static void render();
  static bool isAnimatingPush;
  static bool isAnimatingPop;
};
#endif
