#include <iostream>
#include "scene.h"
#include "../conveyor/conveyor.h"
#include <math.h>
#include "GL/freeglut.h"
#include "../utils/GUtils.h"
#include "GL/gl.h"
#include "../queue/queue.h"
#include "../utils/GameUtils.h"
#include "../orders/orders.h"
#include "../screen/screen.h"

Conveyor c1;
Queue q1;
point plateXY = {0.0, -0.37};
Orders o1;

void drawPastaManBottom();
void drawConveyorBottom();
void drawTimer();

void Scene::render()
{
    drawBackground();
    drawConveyorBottom();
    c1.render();
    GUtils::drawPlate(plateXY);
    drawPastaManBottom();
    GameUtils::loadFood();
    q1.render();
    drawPastaMan();
    drawOrderQueue();
    o1.render();
    drawStackPlatter();
    drawScore();
    drawTimer();
}

void Scene::drawScore()
{
    GUtils::color(255, 255, 255);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH, GL_NICEST);
    glLineWidth(3.0);
    std::string txt = "Score : " + std::to_string(GameUtils::getScore());
    point p = {-1.4, -1.78};
    GUtils::drawStrokedText(p, txt, 0.002);
}

GLfloat Scene::timer = 1.0685;

void drawTimer()
{
    glLineWidth(10.0);
    // Scene::timer -= 0.000181 * GUtils::GAMESPEED;
    Scene::timer -= 0.000581 * GUtils::GAMESPEED;

    GUtils::color(100, 100, 100);
    glBegin(GL_LINES);
    glVertex2f(-2.67, -2.0);
    glVertex2f(1.0685, -2.0);
    glEnd();

    glColor3f(0, 1, 0);
    if (Scene::timer < (GLfloat)-2.67 / (GLfloat)4.0)
        glColor3f(1.0, 0.64, 0.0);
    if (Scene::timer < (GLfloat)-2.67 / (GLfloat)1.5)
        glColor3f(1.0, 0, 0.0);

    if (Scene::timer > -2.67)
    {
        glBegin(GL_LINES);
        glVertex2f(-2.67, -2.0);
        glVertex2f(Scene::timer, -2.0);
        glEnd();
        GameUtils::isTimerRunning = true;
    }
    else
    {
        std::cout << "GAME OVER" << std::endl;
        GameUtils::isTimerRunning = false;
        Screen::switchEndScreen();
    }
}

void drawConveyorBottom()
{
    // glColor4f(0.37890625, 0.37890625, 0.37890625, 0.5);
    GUtils::color(117, 117, 117);
    point a = {-2.67, -0.85};
    point b = {-2.67, -2.25};
    point c = {1.0685, -2.25};
    point d = {1.0685, -0.85};
    GUtils::drawQuad(a, b, c, d);
}

void Scene::drawStackPlatter()
{
    GUtils::color(220, 220, 220);
    GUtils::drawEllipse(0.5, 0.9, 2.0, -1.15);
    GUtils::color(192, 192, 192);
    GUtils::drawEllipse(0.4, 0.8, 2.0, -1.15);
}

void Scene::drawOrderQueue()
{ //coordinates to draw the rope
    GLfloat r1[] = {-4.0, 1.75};
    GLfloat r2[] = {1.9, 1.75};

    //drawing rope for orders
    GUtils::color(000, 000, 000);
    glBegin(GL_LINES);
    glLineWidth(7.0);
    glVertex2fv(r1);
    glVertex2fv(r2);
    glEnd();

    //coordinates for order card 1
    GLfloat a1[] = {-1.9, 1.8};
    GLfloat a2[] = {-1.3, 1.8};
    GLfloat a3[] = {-1.3, 1.2};
    GLfloat a4[] = {-1.9, 1.2};
    //coordinates for order card 2
    GLfloat b1[] = {-2.7, 1.8};
    GLfloat b2[] = {-2.1, 1.8};
    GLfloat b3[] = {-2.1, 1.2};
    GLfloat b4[] = {-2.7, 1.2};

    //coordinates for order card 3
    GLfloat c1[] = {-3.5, 1.8};
    GLfloat c2[] = {-2.9, 1.8};
    GLfloat c3[] = {-2.9, 1.2};
    GLfloat c4[] = {-3.5, 1.2};

    //drawing order cards
    GUtils::color(249, 241, 241);
    GUtils::drawQuad(a1, a2, a3, a4);
    GUtils::drawQuad(b1, b2, b3, b4);
    GUtils::drawQuad(c1, c2, c3, c4);

    //drawing outline for order cards
    GUtils::color(250, 235, 215);
    GUtils::drawQuadOutline(a1, a2, a3, a4, 5.0);
    GUtils::drawQuadOutline(b1, b2, b3, b4, 5.0);
    GUtils::drawQuadOutline(c1, c2, c3, c4, 5.0);
}

void drawPastaManBottom()
{
    //coordinates for quad below body (bottom hack)
    GLfloat b1[] = {-1.35, -0.23};
    GLfloat b2[] = {-1.35, -1.03};
    GLfloat b3[] = {-0.05, -1.03};
    GLfloat b4[] = {-0.05, -0.23};
    GUtils::color(220, 220, 220);
    //GUtils::drawQuad(b4, b3, b2, b1);
    GUtils::drawQuad(b1, b2, b3, b4);
}

void Scene::drawPastaMan()
{ //coordinates for pasta man's head
    GLfloat head[] = {-0.7, 0.525};

    //coordinates for pasta man's body
    GLfloat body[] = {-0.7, -0.245};

    //coordinates for pasta man's eyes
    GLfloat e1[] = {-0.85, 0.5};
    GLfloat e2[] = {-0.55, 0.5};

    //coordinates for quad below body (top hack)

    GLfloat b1[] = {-1.35, -0.9};
    GLfloat b2[] = {-1.35, -0.5};
    GLfloat b3[] = {-0.05, -0.5};
    GLfloat b4[] = {-0.05, -0.9};

    //coordinates for drawing buttons
    GLfloat bt1[] = {-0.7, -0.15};
    GLfloat bt2[] = {-0.7, -0.38};
    GLfloat bt3[] = {-0.7, -0.6};

    //drawing head and body
    GUtils::color(220, 220, 220);
    GUtils::drawCircle(body, 0.65);
    GUtils::color(255, 218, 185);
    GUtils::drawCircle(head, 0.45);
    GUtils::color(220, 220, 220);
    GUtils::drawQuad(b1, b2, b3, b4);

    //drawing eyes
    GUtils::color(000, 000, 000);
    GUtils::drawCircle(e1, 0.05);
    GUtils::drawCircle(e2, 0.05);

    //drawing buttons
    GUtils::color(47, 79, 79);
    GUtils::drawCircle(bt1, 0.05);
    GUtils::drawCircle(bt2, 0.05);
    GUtils::drawCircle(bt3, 0.05);

    //coordinates for hat rectangle
    GLfloat h1[] = {-1.1, 1.2};
    GLfloat h2[] = {-0.3, 1.2};
    GLfloat h3[] = {-0.3, 0.7};
    GLfloat h4[] = {-1.1, 0.7};

    //drawing hat
    GUtils::color(204, 204, 204);
    GUtils::drawQuad(h1, h2, h3, h4);
    GUtils::drawEllipse(0.6, 0.25, -0.7, 1.1);
    GUtils::color(169, 169, 169);
    GUtils::drawEllipse(0.05, 0.1, -1.01, 1.11);
    GUtils::drawEllipse(0.05, 0.1, -0.7, 1.11);
    GUtils::drawEllipse(0.05, 0.1, -0.4, 1.11);

    //coordinates for mouch
    GLfloat m1[] = {-0.85, 0.4};
    GLfloat m2[] = {-0.55, 0.4};
    GLfloat m3[] = {-0.51, 0.3};
    GLfloat m4[] = {-0.89, 0.3};
    //drawing mouch
    GUtils::color(220, 20, 60);
    GUtils::drawEllipse(0.1, 0.05, -0.7, 0.3);
    GUtils::color(0, 0, 0);
    GUtils::drawQuad(m1, m2, m3, m4);
}

void Scene::drawBackground()
{
    /*-----------------lower right section-----------------*/
    //brown bits coordinates
    //right most drawer
    GLfloat a1[] = {2.4, 0.0};
    GLfloat a2[] = {4.0, 0.0};
    GLfloat a3[] = {4.0, -0.7};
    GLfloat a4[] = {2.4, -0.7};

    //top bar
    GLfloat a5[] = {-4.0, 0.1};
    GLfloat a6[] = {4.0, 0.1};
    GLfloat a7[] = {4.0, 0.0};
    GLfloat a8[] = {-4.0, 0.0};

    //salmon bits coordinates
    //rightmost drawer
    GLfloat b1[] = {2.4, -0.7};
    GLfloat b2[] = {4.0, -0.7};
    GLfloat b3[] = {4.0, -1.4};
    GLfloat b4[] = {2.4, -1.4};
    //middle section1
    GLfloat b5[] = {0.0, 0.0};
    GLfloat b6[] = {2.4, 0.0};
    GLfloat b7[] = {2.4, -0.7};
    GLfloat b8[] = {0.0, -0.7};
    //middle section 2
    GLfloat b9[] = {0.0, -1.4};
    GLfloat b10[] = {2.4, -1.4};
    GLfloat b11[] = {2.4, -2.3};
    GLfloat b12[] = {0.0, -2.3};

    //yello bits coordinates
    //rightmost drawer
    GLfloat c1[] = {2.4, -1.4};
    GLfloat c2[] = {4.0, -1.4};
    GLfloat c3[] = {4.0, -2.3};
    GLfloat c4[] = {2.4, -2.3};
    //middle section
    GLfloat c5[] = {0.0, -0.7};
    GLfloat c6[] = {2.4, -0.7};
    GLfloat c7[] = {2.4, -1.4};
    GLfloat c8[] = {0.0, -1.4};

    /*-----------------lower left section-----------------*/
    //brown cabinet
    GLfloat a9[] = {-1.0, 0.0};
    GLfloat a10[] = {0.0, 0.0};
    GLfloat a11[] = {0.0, -2.3};
    GLfloat a12[] = {-1.0, -2.3};

    //yellow cabinet
    GLfloat c9[] = {-2.0, 0.0};
    GLfloat c10[] = {-1.0, 0.0};
    GLfloat c11[] = {-1.0, -2.3};
    GLfloat c12[] = {-2.0, -2.3};

    //oven
    GLfloat d1[] = {-4.0, 0.0};
    GLfloat d2[] = {-2.0, 0.0};
    GLfloat d3[] = {-2.0, -2.3};
    GLfloat d4[] = {-4.0, -2.3};

    GLfloat d5[] = {-3.9, -0.2};
    GLfloat d6[] = {-2.1, -0.2};
    GLfloat d7[] = {-2.1, -0.5};
    GLfloat d8[] = {-3.9, -0.5};

    GLfloat d9[] = {-3.9, -0.6};
    GLfloat d10[] = {-2.1, -0.6};
    GLfloat d11[] = {-2.1, -2.1};
    GLfloat d12[] = {-3.9, -2.1};

    GLfloat d13[] = {-3.7, -1.0};
    GLfloat d14[] = {-2.3, -1.0};
    GLfloat d15[] = {-2.3, -1.95};
    GLfloat d16[] = {-3.7, -1.95};

    //oven handle coordinates
    GLfloat d17[] = {-3.7, -0.8};
    GLfloat d18[] = {-2.3, -0.8};
    GLfloat d19[] = {-2.3, -0.9};
    GLfloat d20[] = {-3.7, -0.9};

    //knob coordinates
    GLfloat k1[] = {-3.65, -0.35};
    GLfloat k2[] = {-3.35, -0.35};
    GLfloat k3[] = {-3.05, -0.35};
    GLfloat k4[] = {-2.75, -0.35};
    GLfloat k5[] = {-2.45, -0.35};

    //brown 160,82,45
    GUtils::color(222, 184, 135);
    GUtils::drawQuad(a1, a2, a3, a4);
    GUtils::drawQuad(a9, a10, a11, a12);

    //salmon 250,128,114
    GUtils::color(255, 182, 193);
    GUtils::drawQuad(b1, b2, b3, b4);
    GUtils::drawQuad(b5, b6, b7, b8);
    GUtils::drawQuad(b9, b10, b11, b12);

    //yellow 255,215,0
    GUtils::color(240, 230, 140);
    GUtils::drawQuad(c1, c2, c3, c4);
    GUtils::drawQuad(c5, c6, c7, c8);
    GUtils::drawQuad(c9, c10, c11, c12);

    //oven
    GUtils::color(245, 222, 179);
    GUtils::drawQuad(d1, d2, d3, d4);

    GUtils::color(169, 169, 169);
    GUtils::drawQuad(d5, d6, d7, d8);
    //draw knobs
    GUtils::color(105, 105, 105);
    GUtils::drawCircle(k1, 0.1);
    GUtils::drawCircle(k2, 0.1);
    GUtils::drawCircle(k3, 0.1);
    GUtils::drawCircle(k4, 0.1);
    GUtils::drawCircle(k5, 0.1);

    GUtils::color(135, 206, 235);
    GUtils::drawQuad(d9, d10, d11, d12);

    GUtils::color(230, 230, 250);
    GUtils::drawQuad(d13, d14, d15, d16);

    GUtils::color(100, 149, 237);
    GUtils::drawQuad(d17, d18, d19, d20);

    //draw handles
    GUtils::color(0, 0, 0);
    GUtils::drawEllipse(0.13, 0.05, 3.2, -0.35);
    GUtils::drawEllipse(0.13, 0.05, 3.2, -1.05);
    GUtils::drawEllipse(0.13, 0.05, 1.2, -0.35);
    GUtils::drawEllipse(0.13, 0.05, 1.2, -1.85);
    GUtils::drawEllipse(0.13, 0.05, 3.2, -1.85);
    GUtils::drawEllipse(0.13, 0.05, 1.2, -1.05);

    glBegin(GL_LINES);
    GUtils::color(160, 82, 45);
    glVertex2f(-0.5, 0.0);
    glVertex2f(-0.5, -2.3);
    glEnd();

    glBegin(GL_LINES);
    GUtils::color(255, 215, 0);
    glVertex2f(-1.5, 0.0);
    glVertex2f(-1.5, -2.3);
    glEnd();

    /*-----------------top left section-----------------*/

    GLfloat top1[] = {-4.0, 2.3};
    GLfloat top2[] = {4.0, 2.3};
    GLfloat top3[] = {4.0, 0.0};
    GLfloat top4[] = {-4.0, 0.0};
    GUtils::color(255, 228, 225);
    GUtils::drawQuad(top1, top2, top3, top4);

    //coordinates for stove
    GLfloat s1[] = {-4.0, 0.25};
    GLfloat s2[] = {-2.0, 0.25};
    GLfloat s3[] = {-2.0, 0.15};
    GLfloat s4[] = {-4.0, 0.15};

    GLfloat s5[] = {-3.8, 0.3};
    GLfloat s6[] = {-3.2, 0.3};
    GLfloat s7[] = {-3.2, 0.25};
    GLfloat s8[] = {-3.8, 0.25};

    GLfloat s9[] = {-2.8, 0.3};
    GLfloat s10[] = {-2.2, 0.3};
    GLfloat s11[] = {-2.2, 0.25};
    GLfloat s12[] = {-2.8, 0.25};

    //pan on stove 1
    GLfloat p1[] = {-3.9, 0.71};
    GLfloat p2[] = {-3.1, 0.71};
    GLfloat p3[] = {-3.1, 0.31};
    GLfloat p4[] = {-3.9, 0.31};

    //pan on stove 2
    GLfloat p5[] = {-2.9, 0.51};
    GLfloat p6[] = {-2.1, 0.51};
    GLfloat p7[] = {-2.1, 0.31};
    GLfloat p8[] = {-2.9, 0.31};

    //counter
    GUtils::color(153, 153, 255);
    GUtils::drawQuad(a5, a6, a7, a8);

    //stove
    GUtils::color(219, 112, 147);
    GUtils::drawQuad(s1, s2, s3, s4);
    GUtils::drawQuad(s5, s6, s7, s8);
    GUtils::drawQuad(s9, s10, s11, s12);

    GUtils::color(221, 160, 221);
    GUtils::drawQuad(p1, p2, p3, p4);

    GUtils::color(176, 224, 230);
    GUtils::drawQuad(p5, p6, p7, p8);

    //drawing pan handles
    GUtils::color(0, 0, 0);
    GUtils::drawEllipse(0.075, 0.05, -3.9, 0.57);
    GUtils::drawEllipse(0.075, 0.05, -3.1, 0.57);
    GUtils::drawEllipse(0.07, 0.04, -2.1, 0.45);
    GUtils::drawEllipse(0.07, 0.04, -2.9, 0.45);

    /*-----------------top left chimney-----------------*/
    GLfloat m1[] = {-3.5, 2.3};
    GLfloat m2[] = {-2.0, 2.3};
    GLfloat m3[] = {-2.0, 1.5};
    GLfloat m4[] = {-3.5, 1.5};
    //lower half of chimney
    GLfloat sh1[] = {-3.5, 1.5};
    GLfloat sh2[] = {-2.0, 1.5};
    GLfloat sh3[] = {-1.7, 1.0};
    GLfloat sh4[] = {-3.8, 1.0};

    //draw pastel green
    GUtils::color(175, 238, 238);
    GUtils::drawQuad(m1, m2, m3, m4);

    //draw shadow 176,196,222
    GUtils::color(176, 196, 222);
    GUtils::drawQuad(sh1, sh2, sh3, sh4);

    /*-----------------top left cabinet-----------------*/

    GLfloat n1[] = {-1.8, 2.3};
    GLfloat n2[] = {-1.3, 2.3};
    GLfloat n3[] = {-1.3, 1.5};
    GLfloat n4[] = {-1.8, 1.5};

    GLfloat n5[] = {-1.3, 2.3};
    GLfloat n6[] = {-0.8, 2.3};
    GLfloat n7[] = {-0.8, 1.5};
    GLfloat n8[] = {-1.3, 1.5};

    GLfloat n9[] = {-0.8, 2.3};
    GLfloat n10[] = {1.0, 2.3};
    GLfloat n11[] = {1.0, 2.1};
    GLfloat n12[] = {-0.8, 2.1};

    GLfloat n13[] = {-0.8, 2.1};
    GLfloat n14[] = {1.0, 2.1};
    GLfloat n15[] = {1.0, 1.5};
    GLfloat n16[] = {-0.8, 1.5};

    GUtils::color(255, 149, 127);
    GUtils::drawQuad(n1, n2, n3, n4);
    GUtils::drawQuad(n13, n14, n15, n16);

    GUtils::color(255, 153, 204);
    GUtils::drawQuad(n9, n10, n11, n12);

    GUtils::color(255, 178, 102);
    GUtils::drawQuad(n5, n6, n7, n8);

    //handles for top cabinets
    GUtils::color(0, 0, 0);
    GUtils::drawEllipse(0.13, 0.05, 0.1, 1.9);
    GUtils::drawEllipse(0.05, 0.05, -1.4, 1.9);
    GUtils::drawEllipse(0.05, 0.05, -1.2, 1.9);

    //coordinates for stove right
    GLfloat ss1[] = {4.0, 0.25};
    GLfloat ss2[] = {2.0, 0.25};
    GLfloat ss3[] = {2.0, 0.15};
    GLfloat ss4[] = {4.0, 0.15};

    GLfloat ss5[] = {3.8, 0.3};
    GLfloat ss6[] = {3.2, 0.3};
    GLfloat ss7[] = {3.2, 0.25};
    GLfloat ss8[] = {3.8, 0.25};

    GLfloat ss9[] = {2.8, 0.3};
    GLfloat ss10[] = {2.2, 0.3};
    GLfloat ss11[] = {2.2, 0.25};
    GLfloat ss12[] = {2.8, 0.25};

    // pan on stove 1
    GLfloat ps1[] = {3.9, 0.71};
    GLfloat ps2[] = {3.1, 0.71};
    GLfloat ps3[] = {3.1, 0.31};
    GLfloat ps4[] = {3.9, 0.31};

    GLfloat ps11[] = {3.9, 0.61};
    GLfloat ps22[] = {3.1, 0.61};
    GLfloat ps33[] = {3.1, 0.41};
    GLfloat ps44[] = {3.9, 0.41};

    //pan on stove 2
    GLfloat ps5[] = {2.9, 0.51};
    GLfloat ps6[] = {2.1, 0.51};
    GLfloat ps7[] = {2.1, 0.31};
    GLfloat ps8[] = {2.9, 0.31};

    GLfloat ps55[] = {2.2, 0.71};
    GLfloat ps66[] = {2.8, 0.71};
    GLfloat ps77[] = {2.8, 0.51};
    GLfloat ps88[] = {2.2, 0.51};

    GLfloat ps555[] = {2.3, 0.81};
    GLfloat ps666[] = {2.7, 0.81};
    GLfloat ps777[] = {2.7, 0.71};
    GLfloat ps888[] = {2.3, 0.71};

    //stove right
    GUtils::color(255, 178, 102);
    GUtils::drawQuad(ss1, ss2, ss3, ss4);
    GUtils::drawQuad(ss5, ss6, ss7, ss8);
    GUtils::drawQuad(ss9, ss10, ss11, ss12);

    //pans right
    GUtils::color(255, 102, 178);
    GUtils::drawQuad(ps1, ps2, ps3, ps4);
    GUtils::drawQuad(ps55, ps66, ps77, ps88);

    GUtils::color(51, 255, 153);
    GUtils::drawQuad(ps5, ps6, ps7, ps8);
    GUtils::drawQuad(ps555, ps666, ps777, ps888);
    GUtils::drawQuad(ps11, ps22, ps33, ps44);

    /*----------------- microwave -----------------*/

    ;
    GLfloat mic1[] = {0.5, 0.75};
    GLfloat mic2[] = {1.6, 0.75};
    GLfloat mic3[] = {1.6, 0.15};
    GLfloat mic4[] = {0.5, 0.15};

    GLfloat mic11[] = {0.55, 0.7};
    GLfloat mic22[] = {1.3, 0.7};
    GLfloat mic33[] = {1.3, 0.2};
    GLfloat mic44[] = {0.55, 0.2};

    GUtils::color(119, 136, 153);
    GUtils::drawQuad(mic1, mic2, mic3, mic4);

    GUtils::color(0, 0, 0);
    GUtils::drawEllipse(0.05, 0.05, 1.45, 0.6);
    GUtils::drawEllipse(0.05, 0.05, 1.45, 0.45);
    GUtils::drawEllipse(0.05, 0.05, 1.45, 0.29);

    GUtils::color(169, 169, 169);
    GUtils::drawQuad(mic11, mic22, mic33, mic44);
}