#ifndef SCENE
#define SCENE
#include "GL/gl.h"
#include "../utils/GUtils.h"

class Scene
{
public:
  void render();
  static GLfloat timer;

private:
  void drawPastaMan();
  void drawBackground();
  void drawStackPlatter();
  void drawScore();
  void drawOrderQueue();
};

#endif