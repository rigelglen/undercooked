#ifndef QUEUE
#define QUEUE
#include "GL/gl.h"
#include "../utils/GUtils.h"

class Queue
{
public:
  void render();

private:
  void drawQueue();
  void drawQueueLines();
};

class QueueLine
{
public:
  point start = {0.0, 0.0};
  point end = {0.0, 0.0};
  bool isDrawn = false;
  void render();
  void move();
  void setVals(point a, point b);
};

#endif