#include <iostream>
#include "queue.h"
#include <math.h>
#include "GL/freeglut.h"
#include "../utils/GUtils.h"
#include "GL/gl.h"

QueueLine queueLines[4];
point queueLineStart = {2.0, 0.95 + 0.45};
point queueLineEnd = {2.0, 1.6 + 0.45};

void Queue::render()
{
    drawQueue();
    drawQueueLines();
}

void Queue::drawQueue()
{
    point a = {2.0 + 0.1, 1.5 + 0.1 + 0.45};
    point b = {2.0 + 0.1, 1.05 + 0.1 + 0.45};
    point c = {4.0 + 0.15, 1.05 + 0.1 + 0.45};
    point d = {4.0 + 0.15, 1.5 + 0.1 + 0.45};
    // #616161 - grey
    glColor3f(0.37890625, 0.37890625, 0.37890625);
    GUtils::drawRoundedQuad(a, b, c, d, 0.1);
}

void Queue::drawQueueLines()
{
    for (int i = 0; i < 4; i++)
    {
        if (queueLines[i].isDrawn == false)
        {
            queueLines[i].setVals(queueLineStart, queueLineEnd);
        }
        queueLines[i].isDrawn = true;
        queueLines[i].move();
        queueLineStart[0] = queueLineEnd[0] += 0.5;
    }
}

void QueueLine::render()
{
    glLineWidth(GUtils::prepareConveyorScale(2.5));
    // #262626 -- dark grey
    glColor3f(0.1484375, 0.1484375, 0.1484375);
    glBegin(GL_LINES);
    glVertex2fv(start);
    glVertex2fv(end);
    glEnd();
}

void QueueLine::move()
{
    if (start[0] > 4.0)
    {
        start[0] = 1.975;
        end[0] = 1.975;
    }

    start[0] += GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);
    end[0] += GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);
    render();
}

void QueueLine::setVals(point a, point b)
{
    start[0] = a[0];
    start[1] = a[1];
    end[0] = b[0];
    end[1] = b[1];
}