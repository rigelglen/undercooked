#ifndef CONVEYOR
#define CONVEYOR
#include "GL/gl.h"
#include "../utils/GUtils.h"

class Conveyor
{
public:
  void render();
  static void reset();

private:
  void drawConveyor();
  void drawConveyorLines();
  void drawOuterPill();
  void drawInnerPill();
};

class ConveyorLine
{
public:
  point start = {0.0, 0.0};
  point end = {0.0, 0.0};
  bool isDrawn = false;
  GLfloat theta = 0;
  bool isTop = false;
  bool isBottom = false;
  bool isRotTop = false;
  bool isRotBot = false;

  void render();
  void rotateUp();
  void rotateDown();
  void move();
  void setVals(point a, point b);
};

#endif