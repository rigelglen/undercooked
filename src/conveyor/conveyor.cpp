#include <iostream>
#include "conveyor.h"
#include <math.h>
#include "GL/freeglut.h"
#include "../utils/GUtils.h"
#include "GL/gl.h"

void Conveyor::render()
{
    drawConveyor();
    drawConveyorLines();
}

void Conveyor::drawConveyor()
{
    glColor3f(0.37890625, 0.37890625, 0.37890625);
    GUtils::drawPill(3.2, 1.2);
    GUtils::color(193, 193, 193);
    GUtils::drawPill(3.2, 0.3);

    /*
        Conveyor is first drawn as a grey pill with a 
        white pill in the middle to simulate hollowness
    */
}

// 8 lines are maintained for the conveyor belt

ConveyorLine lines[8];

// Starting coordinates from where the lines will be generated from

point lineBottomStart = {GUtils::prepareConveyorX(-1.49), GUtils::prepareConveyorY(-0.15)};
point lineBottomEnd = {GUtils::prepareConveyorX(-1.49), GUtils::prepareConveyorY(-0.6)};

point lineTopStart = {GUtils::prepareConveyorX(-1.49), GUtils::prepareConveyorY(0.15)};
point lineTopEnd = {GUtils::prepareConveyorX(-1.49), GUtils::prepareConveyorY(0.6)};

// Generating positions for the conveyor belt lines

void Conveyor::drawConveyorLines()
{

    // Generating conveyor lines for the bottom half of the conveyor belt

    int i = 0;
    for (i = 0; i < 4; i++)
    {
        // Checking if the line is drawn so that line initialization can happen just once
        if (lines[i].isDrawn == false)
        {
            // Setting starting position of the line
            lines[i].setVals(lineBottomStart, lineBottomEnd);

            //Setting these lines to be in the bottom
            lines[i].isBottom = true;
        }
        // Since line initialization is done, set isDrawn flag
        lines[i].isDrawn = true;

        lines[i].move();

        // Incrementing the bottom and top starting points so that lines can be generated with a distance between them
        lineBottomStart[0] = lineBottomEnd[0] += GUtils::prepareConveyorScale(0.99);
    }

    // Generating conveyor lines for the top half of the conveyor belt

    for (; i < 8; i++)
    {
        if (lines[i].isDrawn == false)
        {
            lines[i].setVals(lineTopStart, lineTopEnd);
            lines[i].isTop = true;
        }
        lines[i].isDrawn = true;

        lines[i].move();
        lineTopStart[0] = lineTopEnd[0] += GUtils::prepareConveyorScale(0.99);
    }
}

// Utility method to set the position of a conveyor line

void ConveyorLine::setVals(point a, point b)
{
    start[0] = a[0];
    start[1] = a[1];
    end[0] = b[0];
    end[1] = b[1];
}

// Method to render the conveyor line

void ConveyorLine::render()
{
    // Set the width of the conveyor line
    glLineWidth(GUtils::prepareConveyorScale(2.5));
    // #262626 -- dark grey
    glColor3f(0.1484375, 0.1484375, 0.1484375);
    // Draw the line
    glBegin(GL_LINES);
    glVertex2fv(start);
    glVertex2fv(end);
    glEnd();
}

void ConveyorLine::move()
{
    // Condition for the movement of conveyor line on the bottom half of the conveyor belt

    /* 
        Flags required for movement in bottom half are
        isBottom=true, isTop=false, isRotTop=false, isRotBot=false, isIntermediate=false
    */

    if ((start[0] >= GUtils::prepareConveyorX(-1.63) && start[0] <= GUtils::prepareConveyorX(1.63)) && isRotTop == false && isRotBot == false && isTop == false)
    {
        // Setting y axis coordinates to a constant value because they don't change

        start[1] = GUtils::prepareConveyorY(-0.15);
        end[1] = GUtils::prepareConveyorY(-0.6);

        // Incrementing x axis to move the conveyor line forward

        start[0] += GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);
        end[0] += GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);

        //Rendering the conveyor line

        render();
    }
    /* 
        Once the conveyor line hits the point at which it is supposed to 
        start rotating upwards, we set the necessary flags for it
    */
    else if (isTop == false && isRotBot == false)
    {
        isRotTop = true;
        isBottom = false;
        isTop = false;
    }

    // Conveyor line is rotating upwards

    /* 
        Flags required for upward rotation are
        isBottom=false, isTop=false, isRotTop=true, isRotBot=false, isIntermediate=false
    */

    if (isRotTop == true && isTop == false)
    {
        rotateUp();
    }

    // Condition for the movement of conveyor line on the top half of the conveyor belt

    /* 
        Flags required for movement in top half are
        isBottom=false, isTop=true, isRotTop=false, isRotBot=false, isIntermediate=false
    */

    if ((start[0] >= GUtils::prepareConveyorX(-1.63) && start[0] <= GUtils::prepareConveyorX(1.63)) && isRotTop == false && isTop == true)
    {
        // Setting y axis coordinates to a constant value because they don't change

        start[1] = GUtils::prepareConveyorY(0.15);
        end[1] = GUtils::prepareConveyorY(0.6);

        // Decrementing x axis to move the conveyor line backward

        start[0] -= GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);
        end[0] -= GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);

        //Rendering the conveyor line

        render();
    }
    /* 
        Once the conveyor line hits the point at which it is supposed to 
        start rotating downwards, we set the necessary flags for it
    */
    else if (isTop == true && isRotTop == false)
    {
        isRotTop = false;
        isTop = false;
        isRotBot = true;
        theta = 0;
    }

    // Conveyor line is rotating downwards

    /* 
        Flags required for downward rotation are
        isBottom=false, isTop=false, isRotTop=false, isRotBot=true, isIntermediate=false
    */

    if (isRotBot == true && isTop == false)
    {
        rotateDown();
    }
}

// Method to rotate a conveyor belt downwards

void ConveyorLine::rotateDown()
{
    // Set point to rotate about to be about the same as the circle that is part of the pill

    point rotatePoint = {GUtils::prepareConveyorX(-1.6), GUtils::prepareConveyorY(0.0)};

    // Perform rotation about rotatePoint

    glPushMatrix();
    glTranslatef(rotatePoint[0], rotatePoint[1], 1.0);
    glRotatef(theta, 0.0, 0.0, 1.0);
    glTranslatef(-rotatePoint[0], -rotatePoint[1], 1.0);

    // Render line after rotation
    render();
    glPopMatrix();

    // Checking if line has rotated by 180 degrees if it hasn't, increment it

    if (theta <= 176)
        theta += GUtils::GAMESPEED * 2;
    /*
        Line has rotated by 180 degrees, now reset flags to prepare 
        for movement of lines in the bottom half of the conveyor belt
    */
    else
    {
        isRotBot = false;
        isBottom = true;

        // Reset x positions of the lines

        start[0] = end[0] = GUtils::prepareConveyorX(-1.54);

        // Reset value of theta to prepare for next rotation

        theta = 0;
    }
}

// Method to rotate a conveyor belt upwards

void ConveyorLine::rotateUp()
{
    // Set point to rotate about to be about the same as the circle that is part of the pill

    point rotatePoint = {GUtils::prepareConveyorX(1.6), GUtils::prepareConveyorY(0.0)};

    // Perform rotation about rotatePoint

    glPushMatrix();
    glTranslatef(rotatePoint[0], rotatePoint[1], 1.0);
    glRotatef(theta, 0.0, 0.0, 1.0);
    glTranslatef(-rotatePoint[0], -rotatePoint[1], 1.0);

    // Render line after rotation

    render();
    glPopMatrix();

    // Checking if line has rotated by 180 degrees if it hasn't, increment it

    if (theta <= 176)
        theta += GUtils::GAMESPEED * 2;
    /*
        Line has rotated by 180 degrees, now reset flags to prepare 
        for movement of lines in the top half of the conveyor belt
    */
    else
    {
        isRotTop = false;
        isTop = true;

        // Reset x positions of the lines

        start[0] = end[0] = GUtils::prepareConveyorX(1.59);
        theta = 0;
    }
}

void Conveyor::reset()
{
    lineBottomStart[0] = GUtils::prepareConveyorX(-1.49);
    lineBottomEnd[0] = GUtils::prepareConveyorX(-1.49);

    lineBottomStart[1] = GUtils::prepareConveyorY(-0.15);
    lineBottomEnd[1] = GUtils::prepareConveyorY(0.6);

    lineTopStart[0] = GUtils::prepareConveyorX(-1.49);
    lineTopEnd[0] = GUtils::prepareConveyorX(-1.49);

    lineTopStart[1] = GUtils::prepareConveyorY(0.15);
    lineTopEnd[1] = GUtils::prepareConveyorY(0.6);
}