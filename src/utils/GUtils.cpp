#include <math.h>
#include <iostream>
#include "GUtils.h"
#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/freeglut.h"
#include "SOIL.h"
#include <string>

GLfloat GUtils::conveyorX = -0.8;
GLfloat GUtils::conveyorY = -0.9;
GLfloat GUtils::conveyorScale = 0.85;

GLfloat GUtils::windowWidth;
GLfloat GUtils::windowHeight;

GLfloat GUtils::screenWidth;
GLfloat GUtils::screenHeight;
GLfloat GUtils::GAMESPEED = 2.8;

//Utility function to draw a circle
void GUtils::drawCircle(point center, GLfloat radius)
{
    glPushMatrix();
    glTranslatef(center[0], center[1], 1.0);
    GLUquadric *quad = gluNewQuadric();
    gluQuadricDrawStyle(quad, GLU_FILL);
    gluDisk(quad, 0.0, radius, 128, 1);
    glPopMatrix();
}

void GUtils::drawCircleOutline(point center, GLfloat radius)
{
    glPushMatrix();
    glTranslatef(center[0], center[1], 1.0);
    GLUquadric *quad = gluNewQuadric();
    gluQuadricDrawStyle(quad, GLU_SILHOUETTE);
    gluDisk(quad, 0.0, radius, 64, 1);
    glPopMatrix();
}

//Utility function to draw a triangle
void GUtils::drawTriangle(point a, point b, point c)
{
    glBegin(GL_TRIANGLES);
    glVertex2fv(a);
    glVertex2fv(b);
    glVertex2fv(c);
    glEnd();
}

void GUtils::color(std::string hex)
{
    int r, g, b;
    if (hex.length() == 7)
        hex.erase(0, 1);
    sscanf(hex.c_str(), "%02x%02x%02x", &r, &g, &b);
    color(r, g, b);
}

void GUtils::color(int r, int g, int b)
{
    glColor3f((GLfloat)r / 255.0, (GLfloat)g / 255.0, (GLfloat)b / 255.0);
}

//Utility function to draw a quadrilateral
/*
        Quads must be drawn in this order

        A--------D
        |        |
        |        |
        B--------C
    
    */
void GUtils::drawQuad(point a, point b, point c, point d)
{
    glBegin(GL_QUADS);
    glVertex2fv(a);
    glVertex2fv(b);
    glVertex2fv(c);
    glVertex2fv(d);
    glEnd();
}

void GUtils::drawQuadOutline(point a, point b, point c, point d, GLfloat lineWidth)
{
    glBegin(GL_LINE_LOOP);
    glLineWidth(lineWidth);
    glVertex2fv(a);
    glVertex2fv(b);
    glVertex2fv(c);
    glVertex2fv(d);
    glEnd();
}

//Utility function to draw an oval/ellipse: parameters the x axis radius, y axis radius
void GUtils::drawEllipse(GLfloat radiusX, GLfloat radiusY, GLfloat x, GLfloat y)
{
    int i;
    glBegin(GL_POLYGON);
    for (i = 0; i < 360; i++)
    {
        GLfloat rad = i * DEG2RAD;
        glVertex2f(cos(rad) * radiusX + x,
                   sin(rad) * radiusY + y);
    }
    glEnd();
}

void GUtils::drawEllipseOutline(GLfloat radiusX, GLfloat radiusY, GLfloat x, GLfloat y)
{
    int i;
    glBegin(GL_LINE_STRIP);
    for (i = 0; i < 360; i++)
    {
        GLfloat rad = i * DEG2RAD;
        glVertex2f(cos(rad) * radiusX + x,
                   sin(rad) * radiusY + y);
    }
    glEnd();
}

GLfloat GUtils::prepareConveyorX(GLfloat x)
{

    return x * conveyorScale + conveyorX;
}

GLfloat GUtils::prepareConveyorY(GLfloat y)
{
    return y * conveyorScale + conveyorY;
}

GLfloat GUtils::prepareConveyorScale(GLfloat s)
{
    return s * conveyorScale;
}

void GUtils::drawPlate(point a)
{
    point c = {GUtils::prepareConveyorX(a[0]), GUtils::prepareConveyorY(a[1])};

    glColor3f(1.0, 1.0, 1.0);
    GUtils::drawCircle(c, 0.15);
    // #F5F5F5
    glColor3f(0.95703125, 0.95703125, 0.95703125);

    GUtils::drawCircleOutline(c, 0.14);
    // #F5F5F5
    glColor3f(0.95703125, 0.95703125, 0.95703125);

    // #E0E0E0
    glColor3f(0.875, 0.875, 0.875);
    GUtils::drawCircleOutline(c, 0.06);

    // #F5F5F5
    glColor3f(0.95703125, 0.95703125, 0.95703125);
    GUtils::drawCircle(c, 0.05);
}

void GUtils::drawStrokedText(point p, std::string str, GLfloat scale)
{
    glPushMatrix();
    glEnable(GL_LINE_SMOOTH);
    glTranslatef(p[0], p[1], 0);
    glScalef(scale, scale, 1);
    glutStrokeString(GLUT_STROKE_ROMAN, reinterpret_cast<const unsigned char *>((str.c_str())));
    //glutBitmapString(font, reinterpret_cast<const unsigned char *>((str.c_str())));
    glPopMatrix();
}

void GUtils::drawText(point p, std::string str)
{
    glPushMatrix();
    glEnable(GL_LINE_SMOOTH);
    glRasterPos2fv(p);
    glutBitmapString(GLUT_BITMAP_HELVETICA_18, reinterpret_cast<const unsigned char *>((str.c_str())));
    glPopMatrix();
}

void GUtils::drawRoundedQuad(point aa, point bb, point cc, point dd, GLfloat radius)
{

    point a = {aa[0] - radius, aa[1] - radius};
    point b = {bb[0] - radius, bb[1] - radius};
    point c = {cc[0] - radius, cc[1] - radius};
    point d = {dd[0] - radius, dd[1] - radius};

    GUtils::drawQuad(a, b, c, d);

    point c1 = {a[0], a[1]};
    GUtils::drawCircle(c1, radius);

    point c2 = {b[0], b[1]};
    GUtils::drawCircle(c2, radius);

    point c3 = {c[0], c[1]};
    GUtils::drawCircle(c3, radius);

    point c4 = {d[0], d[1]};
    GUtils::drawCircle(c4, radius);

    point patch1a = {a[0] + radius, a[1]};
    point patch1b = {a[0] + radius, b[1]};
    point patch1c = {a[0] - radius, b[1]};
    point patch1d = {a[0] - radius, a[1]};
    GUtils::drawQuad(patch1a, patch1b, patch1c, patch1d);

    point patch2a = {a[0], a[1] + radius};
    point patch2b = {a[0], a[1] - radius};
    point patch2c = {d[0], a[1] - radius};
    point patch2d = {d[0], a[1] + radius};
    GUtils::drawQuad(patch2a, patch2b, patch2c, patch2d);

    point patch3a = {b[0], b[1] + radius};
    point patch3b = {b[0], b[1] - radius};
    point patch3c = {c[0], b[1] - radius};
    point patch3d = {c[0], b[1] + radius};
    GUtils::drawQuad(patch3a, patch3b, patch3c, patch3d);

    point patch4a = {c[0] - radius, a[1]};
    point patch4b = {c[0] - radius, b[1]};
    point patch4c = {c[0] + radius, b[1]};
    point patch4d = {c[0] + radius, a[1]};
    GUtils::drawQuad(patch4a, patch4b, patch4c, patch4d);
}

void GUtils::drawPill(GLfloat length, GLfloat width)
{
    //Center of the circle on the left
    point outerCircleCenter1 = {GUtils::prepareConveyorX(-length / 2), GUtils::prepareConveyorY(0.0)};

    //Center of the circle on the right
    point outerCircleCenter2 = {GUtils::prepareConveyorX(length / 2), GUtils::prepareConveyorY(0.0)};

    //Radius of both circles
    GLfloat outerCircleRadius = GUtils::prepareConveyorScale(width / 2);

    //Drawing the circles
    GUtils::drawCircle(outerCircleCenter1, outerCircleRadius);
    GUtils::drawCircle(outerCircleCenter2, outerCircleRadius);

    //Coordinates of the rectangle
    point r1TopLeft = {GUtils::prepareConveyorX(-length / 2), GUtils::prepareConveyorY(width / 2)};
    point r1TopRight = {GUtils::prepareConveyorX(length / 2), GUtils::prepareConveyorY(width / 2)};
    point r1BottomLeft = {GUtils::prepareConveyorX(-length / 2), GUtils::prepareConveyorY(-width / 2)};
    point r1BottomRight = {GUtils::prepareConveyorX(length / 2), GUtils::prepareConveyorY(-width / 2)};

    //Drawing the rectangle
    GUtils::drawQuad(r1TopLeft, r1BottomLeft, r1BottomRight, r1TopRight);
}

GLuint textures[10];
int texCount = 1;

int GUtils::loadTexture(std::string path)
{
    textures[texCount] = SOIL_load_OGL_texture(
        path.c_str(),
        SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_INVERT_Y | SOIL_LOAD_RGBA);

    /* check for an error during the load process */
    if (0 == textures[texCount])
    {
        printf("SOIL loading error: '%s'\n", SOIL_last_result());
        return 0;
    }
    else
    {
        glGenTextures(texCount, &textures[texCount]);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        return texCount++;
    }
}

void GUtils::drawImage(point a, point b, point c, point d, int id)
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, id);
    glColor3f(1,1,1);
    glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex2fv(b);
    glTexCoord2f(1.0, 0.0);
    glVertex2fv(c);
    glTexCoord2f(1.0, 1.0);
    glVertex2fv(d);
    glTexCoord2f(0.0, 1.0);
    glVertex2fv(a);
    glEnd();
    glDisable(GL_TEXTURE_2D);
}