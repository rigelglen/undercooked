#ifndef SOUNDUTILS
#define SOUNDUTILS
#include "irrKlang/irrKlang.h"

class SoundUtils
{
public:
  static irrklang::ISoundEngine *engine;
  static void playBg();
  static void playDing();
  static void destroy();
  static void toggleMute();
  static void playNegative();
  static void playPickup();
  static void stop();
};
#endif
