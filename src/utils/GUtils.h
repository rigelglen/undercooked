#ifndef GUTILS
#define GUTILS
#include "GL/gl.h"
#include <string>

typedef GLfloat point[2];
#define DEG2RAD 3.14159 / 180.0

class GUtils
{
public:
  static void drawCircle(point center, GLfloat radius);
  static void drawTriangle(point a, point b, point c);
  static void drawQuad(point a, point b, point c, point d);
  static void drawEllipse(GLfloat radiusX, GLfloat radiusY, GLfloat x, GLfloat y);
  static void drawRoundedQuad(point aa, point bb, point cc, point dd, GLfloat radius);
  static void drawPlate(point a);
  static void drawCircleOutline(point center, GLfloat radius);
  static void drawEllipseOutline(GLfloat radiusX, GLfloat radiusY, GLfloat x, GLfloat y);
  static void drawStrokedText(point p, std::string str, GLfloat scale);
  static void drawText(point p, std::string str);
  static void color(std::string hex);
  static void color(int r, int g, int b);
  static void drawPill(GLfloat length, GLfloat width);
  static void drawQuadOutline(point a, point b, point c, point d, GLfloat lineWidth);
  static int loadTexture(std::string path);
  static void drawImage(point a, point b, point c, point d, int id);

  static GLfloat prepareConveyorX(GLfloat x);
  static GLfloat prepareConveyorY(GLfloat y);
  static GLfloat prepareConveyorScale(GLfloat s);

  static GLfloat conveyorScale;
  static GLfloat conveyorX;
  static GLfloat conveyorY;

  static GLfloat windowHeight;
  static GLfloat windowWidth;
  static GLfloat screenWidth;
  static GLfloat screenHeight;

  static GLfloat GAMESPEED;
};
#endif
