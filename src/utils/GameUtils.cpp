#include "GameUtils.h"
#include "GUtils.h"
#include "../food/food.h"
#include "../stack/stack.h"
#include <iostream>
#include "SoundUtils.h"
#include "GL/freeglut.h"
#include "../orders/orders.h"

Food GameUtils::foods[8];
point foodBottomStart = {GUtils::prepareConveyorX(-1.1), GUtils::prepareConveyorY(-0.3)};
point foodTopStart = {GUtils::prepareConveyorX(-1.1), GUtils::prepareConveyorY(0.3)};
bool GameUtils::isTimerRunning=false;
int GameUtils::highScore = 0;

bool GameUtils::isMuted = false;
bool GameUtils::isFullScreen = false;
bool GameUtils::isOrderGenerated = false;
bool GameUtils::isFoodBoxEnabled = false;
bool GameUtils::isIgnoreOrder = false;

int saved_width;
int saved_height;
int GameUtils::score = 0;

void GameUtils::reset(){
    foodBottomStart[0] = GUtils::prepareConveyorX(-1.1);
    foodBottomStart[1] = GUtils::prepareConveyorY(-0.3);
    foodTopStart[0] = GUtils::prepareConveyorX(-1.1);
    foodTopStart[1] = GUtils::prepareConveyorY(0.3);
    GameUtils::isTimerRunning=false;
}

void GameUtils::enterFullScreen()
{
    if (!glutGet(GLUT_FULL_SCREEN))
    {
        saved_width = glutGet(GLUT_WINDOW_WIDTH);
        saved_height = glutGet(GLUT_WINDOW_HEIGHT);
    }
    glutFullScreen();
    if (glutGet(GLUT_FULL_SCREEN))
    {
        std::cout << "Exiting fullscreen" << std::endl;
        GameUtils::isFullScreen = false;
        glutReshapeWindow(saved_width, saved_height);
    }
    else
    {
        std::cout << "Entering fullscreen" << std::endl;
        GameUtils::isFullScreen = true;
        glutReshapeWindow(glutGet(GLUT_SCREEN_WIDTH), glutGet(GLUT_SCREEN_HEIGHT));
    }
}

void GameUtils::loadFood()
{
    int i = 0;
    for (i = 0; i < 3; i++)
    {
        if (foods[i].isDrawn == false)
        {
            foods[i].place(foodBottomStart[0], foodBottomStart[1]);
            foods[i].isBottom = true;
        }
        foods[i].isDrawn = true;

        foods[i].move();
        foodBottomStart[0] += GUtils::prepareConveyorScale(0.99);
    }

    for (; i < 6; i++)
    {
        if (foods[i].isDrawn == false)
        {
            foods[i].place(foodTopStart[0], foodTopStart[1]);
            foods[i].isTop = true;
        }
        foods[i].isDrawn = true;
        foods[i].move();
        foodTopStart[0] += GUtils::prepareConveyorScale(0.99);
    }

    if (foods[6].isDrawn == false)
    {
        foods[6].isTop = false;
        foods[6].isRotTop = true;
        foods[6].isIntermediate = true;
        foods[6].theta = 90;
    }
    foods[6].isDrawn = true;

    foods[6].move();

    if (foods[7].isDrawn == false)
    {
        foods[7].isTop = false;
        foods[7].isRotBot = true;
        foods[7].isIntermediate = true;
        foods[7].theta = -90;
    }
    foods[7].isDrawn = true;
    foods[7].move();
}

void GameUtils::handlePush()
{
    for (int i = 0; i < 8; i++)
    {
        if (GameUtils::foods[i].checkArea() == true && Stack::top < 2)
        {
            Stack::pushStack(i);
            std::cout << i << " was pushed" << std::endl;
            SoundUtils::playPickup();
            return;
        }
        if (i == 7)
        {
            SoundUtils::playNegative();
        }
    }
}

bool nonRepeatFlag = false;

void GameUtils::handlePopToQueue()
{
    if (Orders::checkOrders() == true || isIgnoreOrder == true)
    {
        if (Stack::top == -1)
        {
            SoundUtils::playNegative();
        }
        for (int i = Stack::top; i >= 0; i--)
        {

            if (Stack::popStack(true))
            {
                if (Stack::isAnimatingPush == false)
                {
                    SoundUtils::playDing();
                    if (nonRepeatFlag == false)
                    {
                        incScore();
                        nonRepeatFlag = true;
                    }
                    if (i == 0)
                        Orders::generateOrders();
                }
            }
            else
            {
                SoundUtils::playNegative();
            }
        }
        nonRepeatFlag = false;
    }
    else
    {
        SoundUtils::playNegative();
    }
}

void GameUtils::handlePop()
{
    if (Stack::popStack(false))
    {
        SoundUtils::playPickup();        
    }
    else
    {
        SoundUtils::playNegative();
    }
}

void GameUtils::handleClear()
{ 
    while (Stack::popStack(false)){
        SoundUtils::playPickup();
    }
}

void GameUtils::incScore()
{
    score++;
}

int GameUtils::getScore()
{
    return score;
}

void GameUtils::resetScore()
{
    score = 0;
}