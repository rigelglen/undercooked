#include <iostream>
#include "SoundUtils.h"
#include "irrKlang/irrKlang.h"
#include "GameUtils.h"

irrklang::ISoundEngine *SoundUtils::engine;

void checkSetup()
{
    if (!SoundUtils::engine)
    {
        SoundUtils::engine = irrklang::createIrrKlangDevice();
    }

    if (!SoundUtils::engine)
    {
        std::cout << "Could not startup sound engine\n";
        return; // error starting up the engine
    }
}

void SoundUtils::playBg()
{
    checkSetup();
    engine->play2D("../../media/music/background.ogg", false);
}

void SoundUtils::stop(){
    checkSetup();
    engine->stopAllSounds();
}

void SoundUtils::playDing()
{
    engine->play2D("../../media/music/ding.ogg", false);
}

void SoundUtils::playPickup()
{
    engine->play2D("../../media/music/pickup.ogg", false);
}

void SoundUtils::playNegative()
{
    engine->play2D("../../media/music/negative.ogg", false);
}

void SoundUtils::toggleMute()
{
    if (engine->getSoundVolume() != 0.0)
    {
        std::cout << "Sound muted" << std::endl;
        engine->setSoundVolume(0.0);
        GameUtils::isMuted = true;
    }
    else
    {
        std::cout << "Sound unmuted" << std::endl;
        engine->setSoundVolume(1.0);
        GameUtils::isMuted = false;
    }
}

void SoundUtils::destroy()
{
    checkSetup();
    engine->drop();
}