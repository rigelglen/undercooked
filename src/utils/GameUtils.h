#ifndef GAMEUTILS
#define GAMEUTILS
#include "GL/gl.h"
#include "GUtils.h"
#include "../food/food.h"

class GameUtils
{
public:
  static bool isFullScreen;
  static bool isMuted;
  static bool isFoodBoxEnabled;
  static bool isIgnoreOrder;

  static Food foods[8];
  static void loadFood();
  static void handlePush();
  static void handlePop();
  static void handlePopToQueue();
  static void handleClear();
  static void enterFullScreen();
  static void incScore();
  static int getScore();
  static void resetScore();
  static void reset();
  static bool isOrderGenerated;
  static bool isTimerRunning;
  static int highScore;
private:
  static int score;
};
#endif