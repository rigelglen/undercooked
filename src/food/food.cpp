#include <iostream>
#include "food.h"
#include "../utils/GUtils.h"
#include <math.h>
#include "GL/freeglut.h"
#include "GL/gl.h"
#include "../lib/random.hpp"
#include "../utils/GameUtils.h"

using Random = effolkronium::random_static;

Food::Food(int fid)
{
    foodID = fid;
}

int Food::getID()
{
    return savedID;
}

Food::Food()
{
    foodID = Random::get(1, 6);
}

void Food::reload()
{
    foodID = Random::get(1, 6);
    savedID = foodID;
}

void Food::place(GLfloat xx, GLfloat yy)
{
    x = xx;
    y = yy;
}

bool Food::checkArea()
{
    if (x > GUtils::prepareConveyorX(-0.25) && x < GUtils::prepareConveyorX(0.25) && isTop == false && foodID != 0)
    {
        return true;
    }
    return false;
}

void Food::toggleConveyor()
{
    savedID = foodID;
    foodID = 0;
}

void Food::render()
{
    if (isDrawn == true)
    {
        if (GameUtils::isFoodBoxEnabled == true)
        {
            glColor3f(0, 0, 0);
            glBegin(GL_QUADS);
            glVertex2f(x + (GLfloat)-0.04 * scaleFactor - 0.34 * scaleFactor, y + (GLfloat)0.35 * scaleFactor + 0.225 * scaleFactor);
            glVertex2f(x + (GLfloat)-0.04 * scaleFactor - 0.34 * scaleFactor, y + (GLfloat)0.35 * scaleFactor - 0.225 * scaleFactor - 0.175);
            glVertex2f(x + (GLfloat)-0.04 * scaleFactor + 0.385 * scaleFactor, y + (GLfloat)0.35 * scaleFactor - 0.225 * scaleFactor - 0.175);
            glVertex2f(x + (GLfloat)-0.04 * scaleFactor + 0.385 * scaleFactor, y + (GLfloat)0.35 * scaleFactor + 0.225 * scaleFactor);
            glEnd();
        }
        glPushMatrix();
        switch (foodID)
        {
        case 0:
            //do nothing
            break;
        case 1:
            drawApple();
            break;
        case 2:
            drawOrange();
            break;
        case 3:
            drawPear();
            break;
        case 4:
            drawPizza();
            break;
        case 5:
            drawIceCream();
            break;
        case 6:
            drawGrapes();
            break;
        }
        glPopMatrix();
    }
}
void Food::drawApple()
{

    GLfloat s1[] = {x + (GLfloat)-0.04 * scaleFactor, y + (GLfloat)0.20 * scaleFactor + (GLfloat)0.015};
    GLfloat s2[] = {x + (GLfloat)-0.04 * scaleFactor, y + (GLfloat)0.45 * scaleFactor + (GLfloat)0.015};
    GLfloat s3[] = {x + (GLfloat)0.24 * scaleFactor, y + (GLfloat)0.45 * scaleFactor + (GLfloat)0.015};

    GLfloat c1[] = {x + (GLfloat)-0.14 * scaleFactor, y + (GLfloat)0.0 * scaleFactor + (GLfloat)0.015};
    GLfloat c2[] = {x + (GLfloat)0.11 * scaleFactor, y + (GLfloat)0.0 * scaleFactor + (GLfloat)0.015};

    glColor3f(0.0, 1.0, 0.0);
    GUtils::drawTriangle(s1, s2, s3);
    glColor3f(1.0, 0.0, 0.0);
    GUtils::drawCircle(c1, 0.25 * scaleFactor);
    GUtils::drawCircle(c2, 0.25 * scaleFactor);
}

void Food::drawOrange()
{
    GLfloat c1[] = {x + (GLfloat)0.0 * scaleFactor - (GLfloat)0.01, y + (GLfloat)0.0 * scaleFactor + (GLfloat)0.02};
    //coordinates for stalk
    GLfloat s1[] = {x + (GLfloat)0.015 * scaleFactor - (GLfloat)0.01, y + (GLfloat)0.20 * scaleFactor + (GLfloat)0.02};
    GLfloat s2[] = {x + (GLfloat)0.015 * scaleFactor - (GLfloat)0.01, y + (GLfloat)0.45 * scaleFactor + (GLfloat)0.02};
    GLfloat s3[] = {x + (GLfloat)0.2 * scaleFactor - (GLfloat)0.01, y + (GLfloat)0.45 * scaleFactor + (GLfloat)0.02};

    glColor3f(0.0, 1.0, 0.0);
    GUtils::drawTriangle(s1, s2, s3);
    glColor3f(1.0, 0.64, 0.0);
    GUtils::drawCircle(c1, 0.25 * scaleFactor);
}

void Food::drawPizza()
{
    GLfloat c1[] = {x + (GLfloat)0.0 * scaleFactor - (GLfloat)0.006, y + (GLfloat)0.0 * scaleFactor + (GLfloat)0.06};
    //coordinates for pizza pepperoni(2)
    GLfloat p1[] = {x + (GLfloat)0.1 * scaleFactor - (GLfloat)0.006, y + (GLfloat)0.06 * scaleFactor + (GLfloat)0.06};
    GLfloat p2[] = {x + (GLfloat)0.0 * scaleFactor - (GLfloat)0.006, y + (GLfloat)-0.06 * scaleFactor + (GLfloat)0.06};

    GLfloat p3[] = {x + (GLfloat)-0.1 * scaleFactor - (GLfloat)0.006, y + (GLfloat)0.06 * scaleFactor + (GLfloat)0.06};
    //double p4[] = {x + 0.1, y + -0.06};

    glColor3f(0.99, 0.85, 0.72);
    GUtils::drawCircle(c1, 0.25 * scaleFactor);
    glColor3f(1.0, 0.0, 0.0);
    GUtils::drawCircle(c1, 0.2 * scaleFactor);

    glColor3f(0.5, 0.0, 0.0);
    GUtils::drawCircle(p1, 0.05 * scaleFactor);
    GUtils::drawCircle(p2, 0.05 * scaleFactor);
    GUtils::drawCircle(p3, 0.05 * scaleFactor);
}

void Food::drawPear()
{ //coordinates for stalk

    GLfloat s1[] = {x + (GLfloat)0.015 * scaleFactor - (GLfloat)0.005, y + (GLfloat)0.35 * scaleFactor};
    GLfloat s2[] = {x + (GLfloat)0.015 * scaleFactor - (GLfloat)0.005, y + (GLfloat)0.55 * scaleFactor};
    GLfloat s3[] = {x + (GLfloat)0.2 * scaleFactor - (GLfloat)0.005, y + (GLfloat)0.55 * scaleFactor};

    GLfloat pr1[] = {x + (GLfloat)0.0 * scaleFactor - (GLfloat)0.005, y + (GLfloat)0.0 * scaleFactor};
    GLfloat pr2[] = {x + (GLfloat)0.0 * scaleFactor - (GLfloat)0.005, y + (GLfloat)0.25 * scaleFactor};

    glColor3f(0.54, 0.25, 0.07);
    GUtils::drawTriangle(s1, s2, s3);

    glColor3f(0.0, 1.0, 0.0);
    GUtils::drawCircle(pr1, 0.25 * scaleFactor);
    GUtils::drawCircle(pr2, 0.17 * scaleFactor);
}

void Food::drawGrapes()
{
    //grape will have six grapes.
    GLfloat g1[] = {x + (GLfloat)-0.2 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.35 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};
    GLfloat g2[] = {x + (GLfloat)0.0625 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.35 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};
    GLfloat g3[] = {x + (GLfloat)0.325 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.35 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};

    GLfloat g4[] = {x + (GLfloat)-0.0875 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.06875 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};
    GLfloat g5[] = {x + (GLfloat)0.175 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.06875 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};

    GLfloat g6[] = {x + (GLfloat)0.0625 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)-0.175 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};

    //coordinates for stalk
    GLfloat s1[] = {x + (GLfloat)0.025 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.6875 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};
    GLfloat s2[] = {x + (GLfloat)0.325 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.6875 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};
    GLfloat s3[] = {x + (GLfloat)0.025 * (GLfloat)0.75 * scaleFactor - (GLfloat)0.03, y + (GLfloat)0.50 * (GLfloat)0.75 * scaleFactor + (GLfloat)0.005};

    //drawing stalk
    glColor3f(0.54, 0.25, 0.07);
    GUtils::drawTriangle(s1, s2, s3);

    //drawing the grapes
    glColor3f(0.0, 1.0, 0.0);
    GUtils::drawCircle(g1, 0.1875 * (GLfloat)0.75 * scaleFactor);
    GUtils::drawCircle(g2, 0.1875 * (GLfloat)0.75 * scaleFactor);
    GUtils::drawCircle(g3, 0.1875 * (GLfloat)0.75 * scaleFactor);

    GUtils::drawCircle(g4, 0.1875 * (GLfloat)0.75 * scaleFactor);
    GUtils::drawCircle(g5, 0.1875 * (GLfloat)0.75 * scaleFactor);

    GUtils::drawCircle(g6, 0.1875 * (GLfloat)0.75 * scaleFactor);
}

void Food::drawIceCream()
{
    //coordinates for cone
    GLfloat c1[] = {x + (GLfloat)0.0 * scaleFactor - (GLfloat)0.003, y + (GLfloat)-0.15 * scaleFactor - (GLfloat)0.03};
    GLfloat c2[] = {x + (GLfloat)-0.225 * scaleFactor - (GLfloat)0.003, y + (GLfloat)0.3 * scaleFactor - (GLfloat)0.03};
    GLfloat c3[] = {x + (GLfloat)0.225 * scaleFactor - (GLfloat)0.003, y + (GLfloat)0.3 * scaleFactor - (GLfloat)0.03};
    //coordinates for cream
    GLfloat c4[] = {x + (GLfloat)0.0 * scaleFactor - (GLfloat)0.003, y + (GLfloat)0.35 * scaleFactor - (GLfloat)0.03};

    //draw the cream
    glColor3f(0.99, 0.41, 0.70);
    GUtils::drawCircle(c4, 0.225 * scaleFactor);

    //draw the cone
    glColor3f(0.99, 0.86, 0.67);
    GUtils::drawTriangle(c1, c2, c3);
}
