#ifndef FOOD
#define FOOD
#include "GL/gl.h"
#include "../utils/GUtils.h"

class Food
{
public:
  Food(int foodID);
  Food();
  void render();
  void move();
  bool moveTo(GLfloat xx, GLfloat yy);
  void place(GLfloat xx, GLfloat yy);
  bool isTop = false;
  bool isBottom = true;
  bool isRotTop = false;
  bool isRotBot = false;
  bool isDrawn = false;
  bool isIntermediate = false;
  bool onConveyor = true;
  bool onStack = false;
  int savedID;

  GLfloat theta = 0;
  GLfloat scaleFactor = 0.5 * GUtils::conveyorScale;
  void reload();
  bool checkArea();
  void toggleConveyor();
  GLfloat x = 0.0, y = 0.0, z = 1.0;
  int foodID;
  int getID();
  bool stackAnimStep = false;
  bool translationXDone = false;
  bool translationYDone = false;
  bool isQueueIntermediate = false;
  bool onQueue = false;

private:
  void drawApple();
  void drawOrange();
  void drawGrapes();
  void drawIceCream();
  void drawPizza();
  void drawPear();
  void rotateUp();
  void rotateDown();
};

#endif