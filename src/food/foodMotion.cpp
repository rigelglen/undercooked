#include <iostream>
#include "food.h"
#include "../utils/GUtils.h"
#include <math.h>
#include "GL/freeglut.h"
#include "GL/gl.h"

void Food::move()
{
    if (onConveyor == true)
    {
        if (isIntermediate == true && isRotTop == true)
        {
            point rotatePoint = {GUtils::prepareConveyorX(1.5), GUtils::prepareConveyorY(0.0)};
            x = GUtils::prepareConveyorX(1.5);
            y = GUtils::prepareConveyorY(-0.3);
            glPushMatrix();
            glTranslatef(rotatePoint[0], rotatePoint[1], 1.0);
            glRotatef(theta, 0.0, 0.0, 1.0);
            glTranslatef(-rotatePoint[0], rotatePoint[1], 1.0);
            render();
            glPopMatrix();
            isIntermediate = false;
        }

        if (isIntermediate == true && isRotBot == true)
        {
            point rotatePoint = {GUtils::prepareConveyorX(-1.5), GUtils::prepareConveyorY(0.0)};
            x = GUtils::prepareConveyorX(-1.5);
            y = GUtils::prepareConveyorY(0.3);
            glPushMatrix();
            glTranslatef(rotatePoint[0], rotatePoint[1], 1.0);
            glRotatef(theta, 0.0, 0.0, 1.0);
            glTranslatef(-rotatePoint[0], rotatePoint[1], 1.0);
            render();
            glPopMatrix();
            isIntermediate = false;
            theta = 90;
        }

        if ((x >= GUtils::prepareConveyorX(-1.6) && x <= GUtils::prepareConveyorX(1.5)) && isRotTop == false && isRotBot == false && isTop == false)
        {
            x += GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);
            render();
        }
        else if (isTop == false && isRotBot == false)
        {
            isRotTop = true;
            isBottom = false;
            isTop = false;
        }

        if (isRotTop == true && isTop == false)
        {
            rotateUp();
        }

        if ((x >= GUtils::prepareConveyorX(-1.5) && x <= GUtils::prepareConveyorX(1.5)) && isRotTop == false && isTop == true)
        {
            glPushMatrix();
            glTranslatef(x, y, 0.0);
            glRotatef(180, 0.0, 0.0, 1.0);
            glTranslatef(-x, -y, 0.0);
            x -= GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);
            render();
            glPopMatrix();
        }
        else if (isTop == true && isRotTop == false)
        {
            isRotTop = false;
            isTop = false;
            isRotBot = true;
            theta = 0;
        }

        if (isRotBot == true && isTop == false)
        {
            rotateDown();
        }
    }
    else
    {
        if (x < 5.0)
            x += GUtils::GAMESPEED * GUtils::prepareConveyorScale(0.01);
        render();
    }
}

void Food::rotateDown()
{
    point rotatePoint = {GUtils::prepareConveyorX(-1.5), GUtils::prepareConveyorY(0.0)};
    glPushMatrix();
    glTranslatef(rotatePoint[0], rotatePoint[1], 1.0);
    glRotatef(theta, 0.0, 0.0, 1.0);
    glTranslatef(-rotatePoint[0], -rotatePoint[1], 1.0);
    glPushMatrix();
    glTranslatef(x, y, 0.0);
    glRotatef(180, 0.0, 0.0, 1.0);
    glTranslatef(-x, -y, 0.0);
    render();
    glPopMatrix();
    glPopMatrix();
    if (theta <= 176)
        theta += GUtils::GAMESPEED * 2 * 0.79;
    else
    {
        isRotBot = false;
        isBottom = true;
        x = GUtils::prepareConveyorX(-1.5);
        theta = 0;
        y = GUtils::prepareConveyorY(-0.3);
    }
}

void Food::rotateUp()
{
    point rotatePoint = {GUtils::prepareConveyorX(1.5), GUtils::prepareConveyorY(0.0)};
    glPushMatrix();
    glTranslatef(rotatePoint[0], rotatePoint[1], 1.0);
    glRotatef(theta, 0.0, 0.0, 1.0);
    glTranslatef(-rotatePoint[0], -rotatePoint[1], 1.0);
    render();
    glPopMatrix();
    if (theta <= 180)
        theta += GUtils::GAMESPEED * 2 * 0.82;
    else
    {
        isRotTop = false;
        isTop = true;
        theta = 0;
        x = GUtils::prepareConveyorX(1.45);
        y = GUtils::prepareConveyorY(0.3);
    }
}

bool Food::moveTo(GLfloat xx, GLfloat yy)
{
    glPushMatrix();
    GLfloat deltaX = xx - x;
    GLfloat deltaY = yy - y;
    if (fabs(deltaX) < 0.01f)
    {
        x = xx;
    }
    if (fabs(deltaY) < 0.01f)
    {
        y = yy;
    }
    if (x != xx)
    {
        x += deltaX / 100 * 10 * GUtils::GAMESPEED;
    }
    if (y != yy)
    {
        y += deltaY / 100 * 10 * GUtils::GAMESPEED;
    }
    if (x == xx && y == yy)
    {
        render();
        return true;
    }
    glPopMatrix();
    return false;
}