#include <iostream>
#include "orders.h"
#include "../stack/stack.h"
#include "GL/freeglut.h"
#include "../utils/GUtils.h"
#include "GL/gl.h"
#include "../utils/GameUtils.h"
#include "../lib/random.hpp"

// Array to keep track of food IDs in the orders section

int Orders::orders[3];

// Start of the foods for the order section

GLfloat foodStart = -1.6;

using Random = effolkronium::random_static;

void Orders::render()
{
    // Check to see if order has been generated(for first run only)

    if (GameUtils::isOrderGenerated == true)
    {
        for (int i = 0; i < 3; i++)
        {
            if (foods[i].isDrawn == false)
            {
                foods[i].place(foodStart, 1.45);
                foodStart -= 0.8;
            }

            // Setting order food ID to the generated food ID

            foods[i].foodID = orders[i];
            foods[i].isDrawn = true;
            foods[i].render();
        }
    }
    else
    {
        generateOrders();
    }
}

void Orders::generateOrders()
{
    // Get the first random number and use that as the index for the foods

    int rand1 = Random::get(1, 6);

    // Store the returned food id in orders

    Orders::orders[0] = GameUtils::foods[rand1].foodID;

    // Generate another random number for food 2

    int rand2 = Random::get(1, 6);

    /* 
        Check if the previous number and the new random number are equal, 
        if they are keep generating new random numbers
    */

    while (rand1 == rand2)
    {
        rand2 = Random::get(1, 6);
    }
    Orders::orders[1] = GameUtils::foods[rand2].foodID;

    int rand3 = Random::get(1, 6);

    while (rand3 == rand1 || rand3 == rand2)
    {
        rand3 = Random::get(1, 6);
    }

    Orders::orders[2] = GameUtils::foods[rand3].foodID;

    // Set orders generated flag to true

    GameUtils::isOrderGenerated = true;
}

bool Orders::checkOrders()
{
    // Set the amount of items matched to 0

    int checks[] = {0,0,0};
    int checkNum=0;

    // Looping through the stack array as well as the orders array

    for (int i = 0; i < 3; i++)
    {
        for (int j = 0; j < 3; j++)
        {
            if (Stack::foods[i].foodID == orders[j] && checks[j]!=1)
            {
                // Mark item as checked if matched
                checks[j]=1;
                break;
            }
        }
    }

    // Identify how many items have been matched

    for(int i=0;i<3;i++){
        if(checks[i]==1){
            checkNum+=1;
        }
    }

    // If there are 3 matched items, then order is valid
    if (checkNum == 3)
    {
        return true;
    }
    return false;
}