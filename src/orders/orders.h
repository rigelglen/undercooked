#ifndef ORDERS
#define ORDERS
#include "GL/gl.h"
#include "../utils/GUtils.h"
#include "../food/food.h"

class Orders
{
public:
  void render();
  static void generateOrders();
  static bool checkOrders();
  Food foods[3];
public:
  static int orders[3];
};
#endif