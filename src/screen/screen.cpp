#include <iostream>
#include <fstream>
#include <iomanip>
#include <string.h>
#include <math.h>
#include "GL/freeglut.h"
#include "../utils/GUtils.h"
#include "GL/gl.h"
#include "screen.h"
#include "../stack/stack.h"
#include <time.h>
#include <chrono>
#include "../scene/scene.h"
#include "GL/glext.h"
#include "../utils/GameUtils.h"
#include "../stack/stack.h"
#include "../conveyor/conveyor.h"
#include "../orders/orders.h"
#include "../utils/SoundUtils.h"
#include "../lib/encrypt.h"

#ifdef WIN32
#include <direct.h>
#endif
#include <sys/types.h> // required for stat.h
#include <sys/stat.h>

#ifdef WIN32
#include <windows.h>
#include <winuser.h>
#include <tchar.h>
#include "../lib/wglext.h"
#endif

bool openi = false;
bool endi = false;
bool cnti = false;
bool gamei = false;
bool frameRateChanged = false;
Scene s1;
auto start = std::chrono::system_clock::now();
auto end = std::chrono::system_clock::now();
int inital_time = time(NULL), final_time, frame_count;
bool endImageLoaded = false;
bool openImageLoaded=false;
bool controlImageLoaded=false;
int openImage;
int endImage;
int controlImage; 

int Screen::currentScreen = 0;

#ifdef WIN32
PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXTs = NULL;
#endif

void Screen::drawOpeningScreen()
{
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    
    point b1 = {-4.0, 2.25};
    point b2 = {-4.0, -2.25};
    point b3 = {4.0, -2.25};
    point b4 = {4.0, 2.25};
    GUtils::drawImage(b1, b2, b3, b4, openImage);


    // Draw here

    glFinish();
    glutSwapBuffers();
    glutPostRedisplay();
    glDisable(GL_TEXTURE_2D);
}

void Screen::drawEndScreen()
{
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    point b1 = {-4.0, 2.25};
    point b2 = {-4.0, -2.25};
    point b3 = {4.0, -2.25};
    point b4 = {4.0, 2.25};
    GUtils::drawImage(b1, b2, b3, b4, endImage+3);

    point p1 = {-2.0, 1.15};
    point p2 = {-1.6, 0.15};
    point p3 = {-1.45, -0.55};
    point txtpt = {-1, -1.45};

    //btn.render();

    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH, GL_NICEST);
    glLineWidth(4.5);

    std::string txt = "Your score is " + std::to_string(GameUtils::getScore());
    std::string txt2 = "Highscore is " + std::to_string(GameUtils::highScore);
    std::string btnTxt = "Press R to play again";

    glColor3f(1.0, 1.0, 1.0);
    GUtils::drawStrokedText(p1, "GAME OVER", 0.005);
    glLineWidth(3.5);
    GUtils::drawStrokedText(p2, txt, 0.003);
    GUtils::drawStrokedText(p3, txt2, 0.003);
    glLineWidth(2.0);
    GUtils::drawStrokedText(txtpt, btnTxt, 0.0013);

    glFinish();
    glutSwapBuffers();
    glutPostRedisplay();
    glDisable(GL_TEXTURE_2D);
}

void Screen::drawControlsScreen()
{
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);

    // Draw here

    
    point b1 = {-4.0, 2.25};
    point b2 = {-4.0, -2.25};
    point b3 = {4.0, -2.25};
    point b4 = {4.0, 2.25};
    GUtils::drawImage(b1, b2, b3, b4, controlImage+1);



    glFinish();
    glutSwapBuffers();
    glutPostRedisplay();
    glDisable(GL_TEXTURE_2D);
}

void Screen::drawGameScreen()
{
    glClearColor(1, 1, 1, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    s1.render();
    Stack::render();
    glFinish();
    glutSwapBuffers();
    glutPostRedisplay();
    glDisable(GL_TEXTURE_2D);
    frame_count++;
    final_time = time(NULL);

#ifdef WIN32
    if (frame_count > 80 && frameRateChanged == false)
    {
        wglSwapIntervalEXTs(2);
        frameRateChanged = true;
    }
#endif

    if (final_time - inital_time > 0)
    {
        std::cout << "FPS: " << frame_count / (final_time - inital_time) << std::endl;
        frame_count = 0;
        inital_time = final_time;
    }
}

void Screen::initGameScreen()
{
#ifdef WIN32
    // Init pointers.
    wglSwapIntervalEXTs = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
#endif

    GameUtils::resetScore();
    Scene::timer = 1.0685;

    for (int i = 0; i < 8; i++)
    {
        GameUtils::foods[i].reload();
    }

    GameUtils::reset();
    Conveyor::reset();
    Orders::generateOrders();
    Stack::reset();
    SoundUtils::stop();
    SoundUtils::playBg();
}

void Screen::initOpeningScreen()
{
    // Init code here
    if (openImageLoaded == false)
    {
        openImage = GUtils::loadTexture("../../media/images/coverpage.png");
        std::cout << "Tex is " << openImage << std::endl;
        openImageLoaded = true;
    }

}


std::string dataFileLoc = "../../data/data.bin";

std::string key = "BJBWF";

void Screen::initEndScreen()
{
    if (endImageLoaded == false)
    {
        endImage = GUtils::loadTexture("../../media/images/bg.jpg");
        std::cout << "Tex is " << endImage << std::endl;
        endImageLoaded = true;
    }

    std::fstream outfile(dataFileLoc, std::ios::in);
    if (!outfile)
    {
#ifdef WIN32
        _mkdir("../../data");
#else
        mode_t nMode = 0733;
        mkdir("../../data", nMode);
#endif
        std::cout << "File doesnt exist, creating.. " << std::endl;
        std::ofstream createFile(dataFileLoc);
        outfile.open(dataFileLoc, std::ios::in);
        createFile.close();
    }
    else
    {
        std::cout << "File already exists " << std::endl;
    }

    std::string line;
    std::string enc;
    std::string dec;

    if (outfile.is_open())
    {
        outfile >> line;
        outfile.close();
        std::cout << "Contents of line is " << line << std::endl;
        if (!line.empty())
        {
            dec = decrypt(line, key);
            std::cout << "That is called"
                      << "\n";
            std::cout << "Dec is " << dec << std::endl;
            try
            {
                GameUtils::highScore = stoi(dec);
            }
            catch (int e)
            {
                GameUtils::highScore = 0;
            }
        }
        std::cout << "High score is " << GameUtils::highScore << std::endl;

        if (GameUtils::highScore <= GameUtils::getScore())
        {
            GameUtils::highScore = GameUtils::getScore();
        }
        std::string sc = std::to_string(GameUtils::highScore);
        // char sc[40];
        // sprintf(sc, "%d", GameUtils::highScore);
        enc = encrypt(sc, key);
        outfile.open(dataFileLoc, std::ios::out | std::ios::trunc);
        outfile << enc;
        outfile.close();
    }

    // Init code here
}
void Screen::initControlsScreen()
{
    // Init code here
    if (controlImageLoaded == false)
    {
        controlImage = GUtils::loadTexture("../../media/images/instructions.png");
        std::cout << "Tex is " << controlImage << std::endl;
        controlImageLoaded = true;
    }
}

void Screen::switchOpeningScreen()
{
    currentScreen = 0;
    initOpeningScreen();
    glutDisplayFunc(drawOpeningScreen);
}

void Screen::switchControlsScreen()
{
    currentScreen = 1;
    initControlsScreen();
    glutDisplayFunc(drawControlsScreen);
}

void Screen::switchGameScreen()
{
    currentScreen = 2;
    initGameScreen();
    glutDisplayFunc(drawGameScreen);
}

void Screen::switchEndScreen()
{
    currentScreen = 3;
    initEndScreen();
    glutDisplayFunc(drawEndScreen);
}