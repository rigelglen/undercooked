#ifndef SCREEN
#define SCREEN
#include "GL/gl.h"
#include "../utils/GUtils.h"

class Screen
{
public:
  static int currentScreen;

  static void switchControlsScreen();
  static void switchOpeningScreen();
  static void switchGameScreen();
  static void switchEndScreen();

  static void drawOpeningScreen();
  static void drawEndScreen();
  static void drawControlsScreen();
  static void drawGameScreen();

  static void initOpeningScreen();
  static void initEndScreen();
  static void initControlsScreen();
  static void initGameScreen();
};

#endif