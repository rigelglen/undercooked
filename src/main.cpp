#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "GL/freeglut.h"
#include "food/food.h"
#include <iostream>
#include "GL/gl.h"
#include "scene/scene.h"
#include <chrono>
#include "GL/glu.h"
#include "utils/GameUtils.h"
#include "stack/stack.h"
#include <time.h>
#include "utils/SoundUtils.h"
#include "SOIL.h"
#include "GL/glext.h"
#include "screen/screen.h"

// Coordinate system is 4.0x2.25

#ifdef WIN32
#include <windows.h>
#include <winuser.h>
#include <tchar.h>
#include "lib/wglext.h"

void SetMyProcessDpiAware()
{
  BOOL(__stdcall * pFn)
  (void);
  HINSTANCE hInstance = LoadLibrary(_T("user32.dll"));
  if (hInstance)
  {
    pFn = (BOOL(__stdcall *)(void))GetProcAddress(hInstance, "SetProcessDPIAware");
    if (pFn)
      pFn();
    FreeLibrary(hInstance);
  }
}
#endif
void init(void)
{
  glClearColor(1.0, 0.0, 1.0, 0.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glColor3f(1.0, 1.0, 1.0);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void myReshape(int w, int h)
{
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  if (w <= h)
  {
    w = (GLfloat)h * (GLfloat)16 / (GLfloat)9;
    if (!glutGet(GLUT_FULL_SCREEN))
    {
      glutReshapeWindow(w, h);
      glutLeaveFullScreen();
    }
    glOrtho(-2.25, 2.25, -2.25 * (GLfloat)h / (GLfloat)w, 2.25 * (GLfloat)h / (GLfloat)w, -10.0, 10.0);
    GUtils::windowWidth = 4.5;
    GUtils::windowHeight = 4.5 * (GLfloat)h / (GLfloat)w;
  }
  else
  {
    h = (GLfloat)w * (GLfloat)9 / (GLfloat)16;
    if (!glutGet(GLUT_FULL_SCREEN))
    {
      glutReshapeWindow(w, h);
      glutLeaveFullScreen();
    }
    glOrtho(-2.25 * (GLfloat)w / (GLfloat)h, 2.25 * (GLfloat)w / (GLfloat)h, -2.25, 2.25, -10.0, 10.0);
    GUtils::windowWidth = 4.5 * (GLfloat)((GLfloat)w / (GLfloat)h);
    GUtils::windowHeight = 4.5;
  }
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}
#ifdef WIN32
PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = NULL;
#endif

void keyboard(unsigned char key, int x, int y)
{
  key = tolower(key);

  switch (key)
  {
  case (unsigned char)13:
    GameUtils::enterFullScreen();
    break;
  case 'm':
    SoundUtils::toggleMute();
    break;
  }

  if(Screen::currentScreen == 0){
    switch(key){
      case ' ':
      Screen::switchControlsScreen();
      std::cout<<"Going to controls screen"<<std::endl;
      break;
    }
  }else if (Screen::currentScreen == 2)
  {
    switch (key)
    {
    case ' ':
      GameUtils::handlePush();
      break;
    case 'a':
      GameUtils::handlePop();
      break;
    case 'b':
      GameUtils::handlePopToQueue();
      break;
    case 'c':
      GameUtils::handleClear();
      break;
    }
  }
  else if (Screen::currentScreen == 3)
  {
    switch (key)
    {
    case 'r':
      Screen::switchGameScreen();
      break;
    }
  }else if(Screen::currentScreen == 1){
    switch(key){
      case ' ':
      Screen::switchGameScreen();
      std::cout<<"Going to game screen"<<std::endl;
      break;
    }
  }
}
int main(int argc, char **argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE);
  glutInitWindowPosition(glutGet(GLUT_SCREEN_WIDTH) / 2 - glutGet(GLUT_SCREEN_WIDTH) / (1.5 * 2), glutGet(GLUT_SCREEN_HEIGHT) / 2 - glutGet(GLUT_SCREEN_HEIGHT) / (1.5 * 2));
  glutInitWindowSize(glutGet(GLUT_SCREEN_WIDTH) / 1.5, glutGet(GLUT_SCREEN_HEIGHT) / 1.5);
#ifdef WIN32
  SetMyProcessDpiAware();
#endif
  glutInitDisplayString("rgba double samples=8");
  glutCreateWindow("Undercooked");
  GUtils::screenWidth = glutGet(GLUT_SCREEN_WIDTH);
  GUtils::screenHeight = glutGet(GLUT_SCREEN_HEIGHT);
  std::cout << "Screen res is " << glutGet(GLUT_SCREEN_WIDTH) << "x" << glutGet(GLUT_SCREEN_HEIGHT) << std::endl;
  std::cout << "Window res is " << glutGet(GLUT_WINDOW_WIDTH) << "x" << glutGet(GLUT_WINDOW_HEIGHT) << std::endl;
  glutReshapeFunc(myReshape);
#ifdef WIN32
  // Init pointers.
  wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
  wglSwapIntervalEXT(1);
#endif
  init();
  Screen::switchOpeningScreen();
  glutKeyboardFunc(keyboard);
  glutMainLoop();
  SoundUtils::destroy();
}