BUILD_DIR ?= ./build
SRC_DIRS ?= src
BUILD_UTILS ?= ./buildutils
MKDIR_L := $(shell mkdir -p "build/linux")
MKDIR_P := $(shell mkdir -p "build/src")
MKDIR_C := $(shell mkdir -p "build/src/conveyor" "build/src/food" "build/src/orders" "build/src/queue" "build/src/scene" "build/src/stack" "build/src/utils" "build/src/screen" "build/src/button")
INC_DIRS ?= ./include
LIB_DIR_WIN ?= ./lib/win
LIB_DIR_LINUX ?= ./lib/linux
LIB_DIR_LINUX_ERK ?= ./lib/linux/libIrrKlang.so

ifeq ($(OS), Windows_NT)
	ICON := $(shell windres "undercooked.rc" -O coff -o "build/src/undercooked.res")
	SRCS := $(shell "$(BUILD_UTILS)/gfind.exe" "$(SRC_DIRS)" -name *.cpp)
	OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
	CPPFLAGS ?= $(INC_FLAGS) -Ofast -MMD -MP -L $(LIB_DIR_WIN) -static -static-libgcc -static-libstdc++ -lSOIL -static -lfreeglut -lopengl32 -lglu32 -lstdc++ -lm -std=c++11 -Wall -lirrKlang 
else
	SRCS := $(shell find "$(SRC_DIRS)" -name *.cpp)
	OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
	CPPFLAGS ?= $(INC_FLAGS) -Ofast -MMD -MP -L $(LIB_DIR_LINUX) -lSOIL -lglut -lGL -lGLU -lstdc++ -lm -std=c++11 -Wall -lIrrKlang -pthread -Wl,-rpath,./lib/linux/ -Wl,-rpath,./
	TARGET_EXEC ?= ./linux/Undercooked
endif

DEPS := $(OBJS:.o=.d)

INC_FLAGS := $(addprefix -I,$(INC_DIRS))

ifeq ($(OS), Windows_NT)
	TARGET_EXEC ?= ./win/Undercooked
else
	TARGET_EXEC ?= ./linux/Undercooked
endif

ifeq ($(OS), Windows_NT)
	TARGET_DIR ?= ./build/win/
else
	TARGET_DIR ?= ./build/linux/
endif

ifeq ($(OS), Windows_NT)
	RESOURCE_FILE ?= ./build/src/undercooked.res
endif


$(BUILD_DIR)/$(TARGET_EXEC): $(OBJS)
	gcc $(OBJS) -o $@ $(RESOURCE_FILE) $(LDFLAGS) $(CPPFLAGS)

# c++ source
$(BUILD_DIR)/%.cpp.o: %.cpp
	gcc $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

-include $(DEPS)

.PHONY: run
run: 
	$(shell cd "$(TARGET_DIR)" && "./Undercooked")

clean:
	$(shell rm -rf "$(BUILD_DIR)/src")